
/*
 * analog.c
 *
 *  Created on: Nov 16, 2020
 *      Author: maria
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "analog.h"
#include "freertos/semphr.h"
#include "esp_err.h"
#include "driver/adc.h"

//timer libraries newly added by me
#include "driver/timer.h"
#include "esp_intr_alloc.h"
#include "esp_attr.h"

#include "esp_console.h"
#include "linenoise/linenoise.h"
#include "argtable3/argtable3.h"
#include "regex.h"

#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
//#define LOG_LOCAL_LEVEL ESP_LOG_ERROR

#include "esp_log.h"

#define CHANNELS (8)
//#define CHANNELS1 (2)


#define TAG "ANALOG_DEMO"
#define AD_TASK_STACK_SIZE (1024)
#define AD_TASK_DELAY_MS (100)

#define MOVING_AVERAGE_QUEUE_SIZE (10)
#define MOVING_HYSTERESIS_DELTA (10)

#define BIT_START (1<<0)
#define BIT_STOP (1<<1)
#define BIT_GROUPS ((BIT_START) | (BIT_STOP))

#define MIN(a,b) ((a)<(b)?(a):(b))
#define MAX(a,b) ((a)>(b)?(a):(b))

#define CMD_AD "ad"
#define ARG_START "start"
#define HINT_START "Start A/D conversion"

#define ARG_STOP "stop"
#define HINT_STOP "Stop A/D conversion"

#define HELP_AD "analog base commands\n"\
	            "Example: \n"\
	            "ad --channel 0 start\n"\
				"ad --channel 1 stop\n"\
				"ad --channel 7 show\n"

#define ARG_SHOW "show"
#define HINT_SHOW "Show A/D statement"

#define ARG_CHANNEL "channel"
#define HINT_CHANNEL "channel index"

#define ARG_QUEUE_SIZE "avg queue size"  //newly added
#define HINT_AVG "avg size"       //newly added

#define ARG_DELTA "delta"  //newly added
#define HINT_DELTA "delta value" //newly added

#define ARG_HELP "help"

#define HINT_AD "Some hint for ad command"
#define HINT_HELP "Print this help"

//For 2 channels same structs can be used just by creating 2 elements of arrays for example:  moving_average_t moving_average[2]; for 2 channels in below example
static struct{
	   struct arg_lit *help; //for string
	   struct arg_lit *start;
	   struct arg_lit *stop;
	   struct arg_int *moving_avg_queue_size; //newly added
	   struct arg_int *moving_hyst_delta;    //newly added
	   struct arg_lit *show;
	   struct arg_int *channel; //for int
       struct arg_end *end;
}ad_args;


typedef struct{
	uint16_t delta;
	uint16_t hyst_min;
	uint16_t hyst_max;
}moving_hysteresis_t;

typedef struct {
	uint16_t queue[MOVING_AVERAGE_QUEUE_SIZE];
	uint8_t index;
}moving_average_t;

typedef struct{
	volatile uint8_t running;
	uint32_t conversions;
	uint16_t raw;
	uint16_t normalized;

	moving_average_t moving_average;
	moving_hysteresis_t moving_hysteresis;
}ad_channel_t;

static TaskHandle_t ad_task_handler;
static ad_channel_t ad_channels;

uint8_t avg_queue_size;
uint8_t delta_size;

static int cmd_ad(int argc, char **argv){
	  int nerrors =arg_parse(argc, argv, (void**) &ad_args);

	  //if the user typed help than ad_args.help->count would become > 0 (ad help)
      if(nerrors || argc == 1 || ad_args.help->count>0){
          printf(HELP_AD);
          return 0;
      }


      //if the user typed both the start and stop at the same time than ad_args.start->count
      //& ad_args.stop->count would be > 0
      if(ad_args.start->count >0 && ad_args.stop->count >0){
         printf("Start or Stop? \n");
         return 1;
      }

      //If the user didn't give any value for ADC channel
      if(ad_args.channel->count ==0){
    	 printf("You have to set channel with -c or --channel from %d up to %d \n",0,CHANNELS);
         return 1;
      }

      uint8_t channel=ad_args.channel->ival[0];

      //Newly added
      if(ad_args.moving_avg_queue_size->count==0){
     	 printf("You have to set moving avg queue size with -q from %d up to %d \n",0,MOVING_AVERAGE_QUEUE_SIZE);
         return 1;
      }

      avg_queue_size=ad_args.moving_avg_queue_size->ival[0];

      //Newly added
      if(ad_args.moving_hyst_delta->count==0){
     	 printf("You have to set hysteresis delta value with -d from %d up to %d \n",0,MOVING_HYSTERESIS_DELTA);
         return 1;
      }

      delta_size=ad_args.moving_hyst_delta->ival[0];


      //if the user typed start
      if(ad_args.start->count > 0){
         ad_channels.running=1;
         printf("A/D Conversion starting\n");
         return 0;
      }

      //if the user typed stop
      if(ad_args.stop->count > 0){
         ad_channels.running=0;
         printf("A/D Conversion stopping\n");
         return 0;
      }

      if(ad_args.show->count>0){
         printf("Running: %s, Conversion: %d, "
        		"Last raw value: %d, Last normalized value: %d, Moving avg queue: %d, Moving Delta value: %d \n",
				 ad_channels.running? "true" : "false",
				 ad_channels.conversions,
				 ad_channels.raw,ad_channels.normalized,avg_queue_size,delta_size);
         return 0;

      }

      return 1;
}


//we have to register commands
static void register_ad_cmd(){
       ad_args.help=arg_lit0("hH", ARG_HELP, HINT_HELP);
       ad_args.start=arg_litn("sS", ARG_START, 0, 1, HINT_START);
       ad_args.stop=arg_litn("pP", ARG_STOP, 0, 1, HINT_STOP);
       ad_args.show=arg_litn("wW", ARG_SHOW, 0, 1, HINT_SHOW);
       ad_args.channel=arg_intn("cC", ARG_CHANNEL, "<n>", 0, CHANNELS, HINT_CHANNEL);

       ad_args.moving_avg_queue_size=arg_intn("qQ", ARG_QUEUE_SIZE, "<n>", 0, 10, HINT_AVG); //newly added. Its max value can be 10
       ad_args.moving_hyst_delta=arg_intn("dD", ARG_DELTA, "<n>", 0, 10, HINT_DELTA); //newly added. Its max value can be 10

       ad_args.end=arg_end(0);

       //Creating a command
       const esp_console_cmd_t cmd={
    	      .command=CMD_AD,
			  .help=ARG_HELP,
			  .hint=HINT_AD,
			  .func=&cmd_ad
       };

       //register it
       ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}




static uint16_t get_moving_hysteresis(uint16_t input){
       if(input>ad_channels.moving_hysteresis.hyst_max){
              ad_channels.moving_hysteresis.hyst_max=MIN(ad_channels.moving_hysteresis.hyst_max
              		  +ad_channels.moving_hysteresis.delta,AD_MAX);

              ad_channels.moving_hysteresis.hyst_min=ad_channels.moving_hysteresis.hyst_min-
            		  ad_channels.moving_hysteresis.delta*2;

              return ad_channels.moving_hysteresis.hyst_max;
       }

       if(input<ad_channels.moving_hysteresis.hyst_min){
            if(ad_channels.moving_hysteresis.hyst_min>AD_MIN + ad_channels.moving_hysteresis.delta){
            	ad_channels.moving_hysteresis.hyst_max -= ad_channels.moving_hysteresis.delta;
            	ad_channels.moving_hysteresis.hyst_min -= ad_channels.moving_hysteresis.delta;

            }
            return ad_channels.moving_hysteresis.hyst_min;

       }
       //If neither of the above two conditions satisfy than just return the input
       return input;
}

static uint16_t get_moving_average(uint16_t input){
	   ad_channels.moving_average.queue[ad_channels.moving_average.index]=input;
       ad_channels.moving_average.index=(ad_channels.moving_average.index+1)%avg_queue_size;  //Firstly it was MOVING_AVERAGE_QUEUE_SIZE

       uint32_t sum=0;
       for(int i=0;i<avg_queue_size;i++) //Firstly it was MOVING_AVERAGE_QUEUE_SIZE
    	     sum +=ad_channels.moving_average.queue[i];

       return sum/avg_queue_size; //Firstly it was MOVING_AVERAGE_QUEUE_SIZE

}

//We have to use 2 ADC Channels
//Since we will have two temperature sensors so we will require(2 ADC Channels) also and 2 fn_analog() functions and maybe or not 2 analog_init() functions or maybe just one will be enough
//Pump is connected to some gpio on esp32 so u can write to that gpio by write function something

//It's a freeRTOS task which will run continuously
static void fn_analog(void *args){
	ESP_LOGD(TAG, "Enter analog task");
	for(;;){
		if(ad_channels.running){
			  //ADC1 CHANNEL 6 is on GPIO 34 of ESP Board so connect potentiometer/Thermometer to this pin. We get the raw value from ADC Pin of the microcontroller and apply moving hysterysis and average.  ad_channels.raw will be the digital value of pt100 from the adc pin of the Ucontroller

			  ad_channels.raw=adc1_get_raw(ADC1_CHANNEL_6);
			  ad_channels.normalized=get_moving_average(get_moving_hysteresis(ad_channels.raw));

			  //ESP_LOGD(TAG, "raw value: %d   normalized value: %d",ad_channels.raw,ad_channels.normalized);
		}
        vTaskDelay(pdMS_TO_TICKS(AD_TASK_DELAY_MS));
	}
}


//Everything initialized in this function
void analog_init(){
	  bzero(&ad_channels,sizeof(ad_channel_t));
      adc1_config_width(ADC_WIDTH_BIT_12);
      adc1_config_channel_atten(ADC_CHANNEL_6, ADC_ATTEN_DB_6);

      ad_channels.moving_hysteresis.delta=delta_size;

      //ADC1 CHANNEL 6 is on GPIO 34 so connect potentiometer/Thermometer to this pin
      //Initializing everything in this function
      uint16_t raw=adc1_get_raw(ADC1_CHANNEL_6);
      ad_channels.moving_hysteresis.hyst_min=raw-delta_size;
      ad_channels.moving_hysteresis.hyst_max=raw+delta_size;

  	  register_ad_cmd();
      xTaskCreate(fn_analog, "Analog", AD_TASK_STACK_SIZE, NULL, uxTaskPriorityGet(NULL), ad_task_handler);

}





