/*
 * console.c
 *
 *  Created on: Nov 24, 2020
 *      Author: maria
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/uart.h"
#include "linenoise/linenoise.h"
#include "argtable3/argtable3.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "esp_console.h"
#include "esp_vfs_dev.h"
#include "esp_vfs_fat.h"
#include "console.h"
#include "cmd_system.h"

#if (CONSOLE_TASK_SIZE < 4096)
#error "Console task size must be at least 4096"
#endif

void console_func(void *arg) {
	const char *prompt = LOG_COLOR_I "esp32> " LOG_RESET_COLOR;
	printf("\n"
	           "This is an ESP-IDF console component.\n"
	           "Type 'help' to get the list of commands.\n"
	           "Use UP/DOWN arrows to navigate through command history.\n"
			   "Press TAB when typing command name to auto-complete.\n");

	int probe_status = linenoiseProbe();
	if (probe_status) {
		printf("\n"
		               "Your terminal application does not support escape sequences.\n"
		               "Line editing and history features are disabled.\n"
					   "On Windows, try using Putty instead.\n");
		linenoiseSetDumbMode(1);
#if CONFIG_LOG_COLORS
        /* Since the terminal doesn't support escape sequences,
         * don't use color codes in the prompt.
         */
        prompt = "esp32> ";
#endif //CONFIG_LOG_COLORS

	}
	for(;;) {
	       /* Get a line using linenoise.
	         * The line is returned when ENTER is pressed.
	         */
	        char* line = linenoise(prompt);
	        if (line == NULL) { /* Ignore empty lines */
	            continue;
	        }
	        /* Add the command to the history */
	        linenoiseHistoryAdd(line);
	#if CONFIG_STORE_HISTORY
	        /* Save command history to filesystem */
	        linenoiseHistorySave(HISTORY_PATH);
	#endif

	        /* Try to run the command */
	        int ret;
	        esp_err_t err = esp_console_run(line, &ret);
	        if (err == ESP_ERR_NOT_FOUND) {
	            printf("Unrecognized command\n");
	        } else if (err == ESP_ERR_INVALID_ARG) {
	            // command was empty
	        } else if (err == ESP_OK && ret != ESP_OK) {
	            printf("Command returned non-zero error code: 0x%x (%s)\n", ret, esp_err_to_name(err));
	        } else if (err != ESP_OK) {
	            printf("Internal error: %s\n", esp_err_to_name(err));
	        }
	        /* linenoise allocates line buffer on the heap, so need to free it */
	        linenoiseFree(line);
	}
}

void console_init() {
	fflush(stdout);
	fsync(fileno(stdout));

	setvbuf(stdin, NULL, _IONBF, 0);
	esp_vfs_dev_uart_set_rx_line_endings(ESP_LINE_ENDINGS_CR);
	esp_vfs_dev_uart_set_tx_line_endings(ESP_LINE_ENDINGS_CRLF);

	const uart_config_t uart_config = {
			.baud_rate = CONFIG_ESP_CONSOLE_UART_BAUDRATE,
			.data_bits = UART_DATA_8_BITS,
			.parity = UART_PARITY_DISABLE,
			.stop_bits = UART_STOP_BITS_1,
			.source_clk = UART_SCLK_REF_TICK
	};

	ESP_ERROR_CHECK(uart_driver_install(CONFIG_ESP_CONSOLE_UART_NUM, 256, 0, 0, NULL, 0));
	ESP_ERROR_CHECK(uart_param_config(CONFIG_ESP_CONSOLE_UART_NUM, &uart_config));

	esp_vfs_dev_uart_use_driver(CONFIG_ESP_CONSOLE_UART_NUM);

	esp_console_config_t console_config = {
			.max_cmdline_args = 16,
			.max_cmdline_length = 256,
#if CONFIG_LOG_COLORS
			.hint_color = atoi(LOG_COLOR_CYAN)
#endif
	};

	ESP_ERROR_CHECK_WITHOUT_ABORT(esp_console_init(&console_config));

	linenoiseSetMultiLine(1);
	linenoiseSetCompletionCallback(&esp_console_get_completion);
	linenoiseSetHintsCallback((linenoiseHintsCallback *) (&esp_console_get_hint));
	linenoiseHistorySetMaxLen(CONSOLE_HIST_LINE_SIZE);
#if CONFIG_STORE_HISTORY
	linenoiseHistoryLoad(HISTORY_PATH);
#endif

	esp_console_register_help_command();
	register_system();
	xTaskCreate(console_func, "console_task", CONSOLE_TASK_SIZE, NULL, uxTaskPriorityGet(NULL), NULL);
}

