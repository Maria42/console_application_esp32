/*
 * analog.h
 *
 *  Created on: Nov 16, 2020
 *      Author: maria
 */

#ifndef MAIN_ANALOG_H_
#define MAIN_ANALOG_H_

#include <stdint.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#define AD_MAX (4096)
#define AD_MIN (0)

void analog_init();

BaseType_t analog_get(uint16_t *result, BaseType_t wait_tick);



#endif /* MAIN_ANALOG_H_ */







