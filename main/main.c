/* FreeRTOS Real Time Stats Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <stdio.h>
#include <stdlib.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_err.h"
#include "analog.h"
#include "console.h"
#include "nvs_flash.h"


#define LOG_LOCAL_LEVEL ESP_LOG_ERROR

#include "esp_log.h"


#define TAG "main"


void app_main(void)
{
   esp_err_t ret;
   // Initialize NVS.
   ret = nvs_flash_init();
   if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
	   ESP_ERROR_CHECK(nvs_flash_erase());
	   ret = nvs_flash_init();
   }

   ESP_ERROR_CHECK( ret );

   esp_log_level_set(TAG, ESP_LOG_DEBUG);
   vTaskDelay(pdMS_TO_TICKS(100));
   analog_init();

   //This is from console.h
   console_init();
}
